# Storage Offers by Sites

| # |Titel (Name der Infrastruktur) |Standort |Datum |Beschreibung  |Interfaces und Transferprotokolle|User Management|Nutzerkreis|
|---|----------------------------------------------------------------------------|:----------------------------------------------:|---------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|1  |Archivsystem der RWTH Aachen|  RWTH Aachen, Forschungszentrum Jülich|Abkündigung und Migration bis Ende 2021|Bandarchiv mit georedundanter Spiegelung mit dem Forschungszentrum Jülich |IBM Spectrum Protect |RWTH SSO (Shibboleth), persönliche Zuordnung von Archivknoten, Funktionsaccounts für den direkten Zugriff. SimpleArchive (Abkündigung vsl. Q2 2021) als Minimalistisches FD-Frontend.  |Mitarbeitende der RWTH Aachen|
|2  |RWTHpublications|RWTH Aachen ||Publikationsserver der RWTH Aachen |HTTP, DOI|RWTH SSO (Shibboleth)|Mitarbeitende der RWTH Aachen|
|3  |Integrationssystem Coscine  |RWTH Aachen |Pilotbetrieb seit Okt. 2020|System zur Integration von Storage Systemen in Forschungsdatenprozesse (Bewirtschaftung, Provisionierung, Metadatenmanagement, Durchsuchbarkeit, Identifikation, Nachnutzbarkeit)  |REST-API / HTML, SPARQL, EPIC |RWTH SSO (Shibboleth), DFN-AAI, ORCID  |Mitarbeitende der RWTH Aachen, Kollaborationspartner, Mitarbeitende der Hochschulen in NRW  |
|4  |FDS (Forschungsdatenspeicher) der RWTH Aachen: Objektspeicher|RWTH Aachen |Pilotbetrieb seit Mär. 2020, Produktivbetrieb seit Okt. 2020|Lokal standortredundantes Objekspeichersystem für die kollaborative Nutzung und langfristige Speicherung von Forschungsdaten |RDF-basierte REST-Schnittstelle (produktiv), S3 (produktiv), NFS  |DFN AAI und Freigabe auf Projektebene mit Coscine  |Wissenschaftsgeleitete Projektbewirtschaftung (vgl. Vergabe von HPC Ressourcen) mit Coscine und perspektivisch JARDS. Vergabe von Speicher an Hochschulen in NRW im Rahmen des Versorgungskonzepts des Landes. Offiziell auch für die Nutzung im Kontext der NFDI Konsortien freigegeben.|
|5  |FDS (Forschungsdatenspeicher) der RWTH Aachen: Fileserver |RWTH Aachen |Pilotbetrieb seit Okt. 2020, Produktivbetrieb vsl. Jun. 2021|Lokal Standortredundantes Fileserver System |NFS, SMB |DFN AAI und Freigabe auf Projektebene mit Coscine  |Wissenschaftsgeleitete Projektbewirtschaftung (vgl. Vergabe von HPC Ressourcen) mit Coscine und perspektivisch JARDS. Vergabe von Speicher an Hochschulen in NRW im Rahmen des Versorgungskonzepts des Landes. Offiziell auch für die Nutzung im Kontext der NFDI Konsortien freigegeben.|
|6  |FDS.NRW: Forschungsdatenspeicher für Nordrhein-Westfalen  |  RWTH Aachen, Uni Köln und Uni Duisburg Essen  |Pilotbetrieb seit Dez. 2020, Produktivbetrieb vsl. ab Mär. 2021|Georedundantes Objekspeicher-System an drei Standorten in NRW: RWTH Aachen, Uni Köln und Uni Duisburg Essen|RDF-basierte REST-Schnittstelle (produktiv), S3 (produktiv), NFS (technisch möglich)|DFN AAI und Freigabe auf Projektebene mit Coscine  |Wissenschaftsgeleitete Projektbewirtschaftung (vgl. vergabe von HPC Ressourcen) mit Coscine und perspektivisch JARDS. Vergabe von Speicher an Hochschulen in NRW im Rahmen des Versorgungskonzepts des Landes. Offiziell auch für die Nutzung im Kontext der NFDI Konsortien freigegeben.|
|7  |GitLab der RWTH Aachen|RWTH Aachen |seit 2018|Server für die Verwaltung von Quellcodes und andere textbasierter Daten. Mit LFS auch Versionsverwaltung von großen Binärdaten. |HTTPS, GIT  |RWTH SSO (Shibboleth), DFN-AAI|Mitarbeitende und Studierende der RWTH Aachen sowie von Kollaborationspartnern, aufgrund der GitLab Education Lizenzbedingungen ggf. dedizierte Übereinkunft notwendig.|
|8  |Backup (und Archiv)-System der TU Darmstadt|  TU Darmstadt, Zeitkopie an der GU FFM||Primär Bandbasierter Speicher für offine data, Nutzer müssen sich nutzungsbasiert an den Kosten beteiligen |IBM Spectrum Protect |Knoten basierte Verwaltung, Knoten werden auf Antrag eingerichtet |Einrichtungen der TU Darmstadt  |
|9  |TUDatalib |TU Darmstadt||Forschungsdaten Repositorium, Technisch: DSpace -swift-> Spectrum Scale Disk -> TSM|HTTP, DOI, Schnittstelle zu CKAN |TUZ-ID|Einrichtungen der TU Darmstadt  |
|10 |HPC-Storage  |TU Darmstadt||Paralleles Dateisystem der Lichtenberg-Cluster auf Basis von Spectrum Scale, Nutzung über HPC-Bewirtschaftung, auch reine Storage-Anträge also ohne CPU Ressourcen sind möglich.|ssh|Nutzermanagement der TU + Freischaltung nach Antrag|Einrichtungen der TU Darmstadt, Externe  |
|11 |Hessenbox-DA |JLU Giessen ||Sync und Share Dienst  |HTTPS + Eigenes Protokoll via APP|Protokoll via APPNutzermanagement der TU Darmstadt + LDAP|Mitarbeitende und Studierende der TU Darmstadt + Eingeladene Partner|
|12 |TUdmo  |TU Darmstadt||Werkzeug zur Erstellung und Pflege eines Datenmanagementplans (DMP) |HTTP  |TU-ID und DFN-AAI |Mitarbeitende und Studierende der TU Darmstadt , Kollaborationspartner |
|13 |Large Scale Data Facility (“LSDF Online Storage”)|Karlsruher Institut für Technologie ||Datenspeicher für große wissenschaftliche Daten. Zuteilung von Ressourcen in Form von Speicherprojektanten |NFS, SMB, SFTP / SSH, lokalere mount HPC, WebDAV / HTTPS |bwIDM, HDF AAI (Unity/Feudal mit ssh-Schlüsseln), Geplant: Einbindung in BW-Datenföderation, OIDC-Zugang (bwIDM/KIT/HDF)|KIT, Land BW, HGF|
|14 |bwDataArchive|Karlsruher Institut für Technologie ||Technische Infrastruktur zur langfristigen Archivierung wissenschaftlicher Daten. Speicherung auch großer Datenbestände für einen Zeitraum von zehn oder mehr Jahren. Bandsystem mit HPSS.  |SFTP  |Eigenes  |Universitäten und Forschungseinrichtungen in BW|
|15 |RADAR4KIT (Research Data Repository for KIT)  |Karlsruher Institut für Technologie ||Dienst zur Archivierung und Publikation von Forschungsdaten aus abgeschlossenen Forschungsprojekten basierend auf RADAR vom FIZ Karlsruhe. Setzt auf bwDataArchive auf.|Webinterface|bwIDM |KIT  |
|16 |Serverbackup |Karlsruher Institut für Technologie ||Serverbackup auf Basis von Spectrum Protect / TSM |Spectrum Protect Client ||KIT  |
|17 |Universität Stuttgart - Datensicherung  |  TIK, Universität Stuttgart  ||Zwei Ausprägungen: 1. Backup - Sicherung des letzten Standes. 2. Archivierung - Sicherung eines bestimmten Zeitpunkts. Bandarchiv mit georedundanter Spiegelung an einen zweiten Standort auf dem Campus. IBM Spectrum Protect  |IBM Spectrum Protect Client|Persönliche Zuordnung von Archivknoten.|Mitarbeitende der Universität Stuttgart  |
|18 |Universität Stuttgart – Fileservice  |  TIK, Universität Stuttgart  ||Bereitstellung von virtuellen Fileservern für Institute (Netapp SVMs). Snapshots zum Wiederherstellen von Daten (bis zu 3 Wochen in die Vergangenheit.) Nächtliche Spiegelung an einen 2. Standort auf dem Campus. Zwei Ausprägungen: • CIFS • NFS|SMB ab Version 2 NFS |CIFS: Zugriff mit zentralem Uni-Account über Active Directory. Die Institute können die Zugriffsrechte selbst über Sicherheitsgruppen regeln. NFS: Zugriffsbeschränkung über Client-Ips|Mitarbeitende der Universität Stuttgart (Zugriff für Studierende möglich) |
|19 |Universität Stuttgart – Datenbanken  |  TIK, Universität Stuttgart  ||TIK-betriebenes Datenbank-Management-System Microsoft® SQL Server® 2019, Einrichtung einer leeren Datenbank und der notwendigen Nutzerlogins bzw. Nutzergruppen, Eigenverwaltung der Datenbanken durch den Datenbankverantwortlichen, Backup und Recovery durch das TIK|ODBC, OLE-DB, JDBC, PHP, etc. |Zugriff mit zentralem Uni-Account über Active Directory. |Mitarbeitende der Universität Stuttgart  |
|20 |Universität Stuttgart – Git-Hosting  |  TIK, Universität Stuttgart  ||Lokaler Github-Enterprise-Server; Lokale Hardware innerhalb der Universität; Source-Code-Verwaltung für Forschung, Lehre und Projekte; Geschlossene Benutzergruppen |Git-Client  |Zugriff mit zentralem Uni-Account über Active Directory  |Mitarbeitende und Studierende der Universität Stuttgart |
|21 |Universität Stuttgart – DaRUS Data Repository of the University of Stuttgart|  TIK, Universität Stuttgart  ||Bietet einen sicheren Ort für Forschungsdaten und Codes, sei es zur Verwaltung der eigenen Daten, zum Austausch innerhalb einer Forschungsgruppe, zur gemeinsamen Nutzung mit ausgewählten Partnern oder zur Veröffentlichung.  |Dataverse GUI, Dataverse API S3  |DFN AAI und Freigabe auf Ebene der Institute |Mitarbeitende und Studierende der Universität Stuttgart. Zugriff für Mitglieder des eduGAIN Verbundes möglich.|
|22 |Universität Stuttgart – Dateitranfer F*EX  |  TIK, Universität Stuttgart  ||Dateien beliebiger Größe an beliebige Empfänger versenden via Browser oder optionalen Clients. |HTTP/HTTPS  |Zentraler Uni-Account.  |Mitarbeitende der Universität Stuttgart. |
|23 |Universität Stuttgart – StorageGrid (Netapp)  |  TIK, Universität Stuttgart  ||Objectstore mit zwei redundanten Standorten.|S3 ||TIK intern. Backend für z.B. Forschungsdatenmanagement und Vorlesungsaufzeichnung. |
|24 |Universität Stuttgart/Universität Hohenheim – bwFAIRDataStorage |TIK, Universität Stuttgart/Universität Hohenheim|In der Antragsphase. Betrieb ab Ende 2023 geplant  |Speichern, metadatieren uind verwalten von Forschungsdaten von der Entstehung bis zur Langzeitspeicherung  |CIFS,NFS,S3 |DFN AAI und Freigabe auf Ebene der Institute |Mitarbeitende und Studierende der Universitäten Stuttgart und Hohenheim. Zugriff für Mitglieder des eduGAIN Verbundes möglich.  |


## User groups

- Within their own institution
- In the federal state
- Within the Helmholtz Association (HGF), MPG, ...
- Cooperation community, e.g. joint project
- NFDI4Ing
- Industry
- National
- EU
	- E.G. EOSC, GAIA-X
- International
- (individual contract)


## Karlsruhe Insitute of Technology - Steinbuch Centre for Computing

### Service "Large Scale Data Facility (LSDF) Online Storage"

- Storage system for actively used large scientific data with direct connection to HPC systems of KIT.
- Access requirements:
	- KIT researchers and their project partners
	- HGF researchers doing climate research
	- Application by KIT researchers

### Service "RADAR4KIT"

- Research data repository, based on RADAR of FIZ
- Access requirement:
	- Available only for KIT researchers as part of the basic IT configuration

### Service "bwDataArchive"

- Service for long-term archiving of scientific data, qualified implementation of the recommendations of the German Research Foundation (DFG) for securing and storing research data.
- Access requirement:
	- Free of charge for KIT researchers as part of the basic IT configuration
	- Universities, colleges and public research institutions from Baden-Württemberg

## Karlsruhe Insitute of Technology - KIT Library

### Service "KITopenData"

- Service for publication, characterization and long-term archiving of research data
- Data are published and archived if they have been created in the context of a publication at a research institution in Baden-Württemberg
- Cooperation with the SCC
- Access requirements:
	- Cooperation between the library of the research institution and KITopenData.
	- Current partner libraries: KIT Library, Freiburg University Library

## RWTH Aachen

### Service "Integration System COSCINE"

- System for integration of storage systems into research data processes (management, provisioning, metadata management, searchability, identification, re-usability).
- Currently, Coscine offers the following resource types:
	- Research Data Storage
		- FDS-Web: The Research Data Store (FDS) is an object-based repository for research data.
		- FDS-S3: The resources can be used via S3 protocol with various tools. For cases where the API can or should be used, a direct S3 interface to the FDS is also available. In this way, data can be written to the FDS in a high-performance and standardized manner.
	- Linked Data: Management of metadata for files in external systems that are not integrated into Coscine.
	- Roadmap: GitLab and Sciebo resources.
- Access requirements:
	- RWTH Aachen University employees
	- Collaboration partners
	- employees of the universities in NRW

### Service "GitLab of RWTH Aachen University"


- Server for managing source code and other text-based data. LFS enables version management of large binary data.
- Access requirements:
	- Staff and students of RWTH Aachen University
	- Collaboration partners, dedicated agreement may be necessary due to GitLab Education license terms

## University of Stuttgart / TIK

### Service "Datenrepositorium (DaRUS)"

- Backup location for research data and codes
- Sharing within the research group or externally with selected partners or for publication
- Easy citation of published datasets
- Access requirements:
	- Provision of a storage area by an organizational unit of the University of Stuttgart
	- Access with an account in the eduGain network (e.g. DFN-AAI)

### Service "Fileservice"

- Setup of an own file server (Storage Virtual Machine)
- Access via CIFS protocol or NFS protocol
- Protection against unintentional deletion or modification of data by snapshots
- Nightly backup of data to a second location
- Access requirements:
	- Organizational units of the University of Stuttgart
	- Account of the University of Stuttgart

### Service "Data Backup"

- Backup - backup of the last status
- Archiving - backup of a specific point in time
- Two copies on tape libraries at different locations
- Access requirement:
	- Organizational unit of the University of Stuttgart

### Service "Databases"

- TIK-operated database management system Microsoft® SQL Server® 2019
- Self-management of the databases by the database manager of the respective institution
- Backup and recovery by the TIK
- Access requirement:
	- Organizational unit of the University of Stuttgart

### Service "Data Transfer F*EX"

- Send large files to any recipient
- Receive large files from any sender
- Access requirement:
	- Account of the Uni Stuttgart

### Service "StorageGrid (Netapp)"

- Objectstore with two redundant locations
- Backend for e.g. research data management (DaRUS) and lecture recording
- Access requirement:
	- TIK internal

## TU Darmstadt

### Service "TUDatalib"

- Research Data Repository
- Technically: DSpace-swift-> Spectrum Scale Disk -> TSM
- Access requirement:
	- TU facilities

# Storage Services in Detail

| Storage Service  | Storage Amount  | Access Protocols| Availability to Collaborators | Persistence of Data  | Geo-Location |
|------------------|--------------------------|--------------------------------------|-----------------------------------------------------------------------------|------------------------------------------|----------------------------------------------|
| **LSDF OS**| 1 TiB to 1 (few) PiB  | SSH/SFTP, WebDAV/HTTPS, SMB/CIFS, NFS | KIT researchers, project partners, Helmholtz Association researchers| For hot and warm research data  | Baden-Württemberg, Germany (KIT) |
| **bwDataArchive**| Agreed upon per project  | SFTP| Employees of KIT, universities and institutions in Baden-Württemberg| 10 years, can be prolonged by request | Baden-Württemberg, Germany (KIT) |
| **Coscine**| Varies by resource type  | WebUI, Coscine API, S3, GitLab API| RWTH Aachen University employees, collaboration partners, universities in NRW | Minimum 10 years | Three locations within NRW, Germany  |
| **RWTH GitLab**  | [Not provided]  | Standard GitLab access and protocols | Staff and students of RWTH Aachen University, collaboration partners| [Not provided]| [Not provided]  |

> Please note that the table above provides a quick overview based on the information available for each service. Some information might be missing or not applicable to certain services.

## LSDF OS

### Confidentiality
- **Provider Access**: Yes
- **Access Limiting Mechanisms**:
  - POSIX FS semantic
  - Every access is authenticated as a user or service account
  - Owner, group, and permission bits on each file and directory
  - ACLs (requires user authentication, can be managed by LSDF admin team)
- **User-side Encryption (Homomorphic Encryption)**:
  - Possible, but no support available

### Storage Amount
- **Range**: 1 TiB to 1 (few) PiB
- **Quota Request**: User must write a request, individual quota agreed upon per project
- **Availability**: KIT researchers, project partners, and Helmholtz Association researchers doing climate research
- **Cost**: According to funding agreements between KIT and funding agencies (MVK, KIT, Helmholtz)

### Access Protocols
- **Public Access**: SSH/SFTP and WebDAV/HTTPS (limited to research institutions' IPs during rollout phase)
- **Internal Access (KIT networks only)**: SMB/CIFS and NFS
- **Direct FS Access on KIT HPC Systems**: bwUniCluster 2.0 and HoreKa

### Connectivity and Accessibility
- **General Performance**: High-throughput and bandwidth (multiple GiB/s), low latency (few ms), medium concurrency (hundreds of clients)
- **Within KIT**: Various protocols
  - SMB/CIFS and NFS for local FS mounts
  - SSH/SFTP for interactive access and transfers
  - WebDAV/HTTPS for transfers, local FS mounts, and direct data access from programs
  - Direct FS access on KIT HPC systems (bwUniCluster 2.0 and HoreKa) access using Jupiter
- **Outside KIT**: Transfer-focused protocols (SSH/SFTP and WebDAV/HTTPS)

### Geo-Location and Privacy
- **Data Centre**: Baden-Württemberg, Germany; owned and operated by KIT

### Regulation and Compliance
- **Compliance and Legal**: Not suitable for personally identifiable data
- **Terms of Use**: [Nutzungsbedingungen LSDF Online Storage](https://www.scc.kit.edu/downloads/sdm/Nutzungsbedingungen-LSDF-Online-Storage.pdf)

### Availability to Collaborators
- **Collaborators**: KIT researchers and project partners can request storage for projects
- **Joining Existing Projects**: Open to all users via Helmholtz AAI

### Persistence of Data
- **Purpose**: For hot and warm research data
- **End Date**: Defined but not currently enforced
- **Not an Archive Storage**

## bwDataArchive

### Service Overview
- Long-term archiving of scientific data
- Qualified implementation of the recommendations of the German Research Foundation (DFG) for securing and storing research data

### Confidentiality
- **Provider Access**: Yes
- **Access Limiting Mechanisms**:
  - POSIX FS semantic
  - Every access is authenticated as a user or service account
  - Owner, group, and permission bits on each file and directory
  - Managed by admins to grant access to service accounts
- **User-side Encryption (Homomorphic Encryption)**:
  - Possible, but no support available

### Storage Amount
- **Quota**: Agreed upon for each project
- **Accounting**: Via home institution
- **Cost**: Free of charge for KIT users as long as they are in their basic package

### Access Protocols
- **SFTP**
- **Maximum Throughput (SFTP, single stream)**: ~70 MB/s

### Geo-Location and Privacy
- **Data Centre**: Baden-Württemberg, Germany; owned and operated by KIT

### Regulation and Compliance
- **Compliance and Legal**
- **Terms of Use**: [Nutzungsordnung bwDataArchive](https://www.rda.kit.edu/nutzungsordnung.php)

### Availability to Collaborators
- **Eligible Users**: Employees of KIT, universities, and institutions in Baden-Württemberg (after creation of a contract)
- **Data Persistence**: Data is still kept and accessible after users have left employment of KIT (this might change in the future)

### Persistence of Data
- **Duration**: 10 years (can be prolonged by request)
- **Deletion**: Data will be deleted after 10 years. A message will be sent 6 months before the deletion time. If no response is received, the data will be deleted.

## RDM Platform Coscine

### Service Overview
- System for integration of storage systems into research data processes (management, provisioning, metadata management, searchability, identification, re-usability)
- Currently offers resource types: Research Data Storage, Linked Data, Gitlab, and a roadmap for Sciebo

### Documentation & Contact

- **Documentation URL**: https://docs.coscine.de/en/
- **Contact URL**: https://coscine.de/contact/
- **Contact E-Mail**: servicedesk@itc.rwth-aachen.de

### Confidentiality
- **Provider Access**: No direct access, but system administrators could potentially access and edit data (except for RDS-WORM resources)
- **User-side Encryption (Homomorphic Encryption)**: Not currently in use

### Storage Amount
- **RDS-Web**: Up to 100 GB for every employee of the RWTH
- **RDS-S3 / RDS-WORM**: Requires application for storage space, members of the RWTH or collaboration partners can apply (min 1 GB, max 512 TB)
- **Linked Data and Gitlab**: No storage limitations within Coscine as it doesn't manage storage directly or resources don't have storage

### Access Protocols
- **All resources**: Available through WebUI or the Coscine API
- **RDS-S3 / RDS-WORM**: Accessible through S3
- **Gitlab**: Accessible through Gitlab platform and API

### Connectivity and Accessibility
- RDS-S3 and RDS-WORM are the most convenient resources for use with the cluster, allowing for easy interaction
- Storing metadata: API can be used for every resource

### Geo-Location and Privacy
- **Data Centre**: Three locations within NRW, Germany
  - 3x Aachen, 1x Cologne, 1x Essen

### Regulation and Compliance
- Research data is stored according to good scientific practice

### Availability to Collaborators
- Eligible Users: RWTH Aachen University employees, collaboration partners, and employees of universities in NRW
- Collaboration: Achieved through Coscine Projects and Coscine Resources with minimum requirement of an ORCID Account

### Persistence of Data
- Duration: At least 10 years
- Backups and Redundancy: Regular backups for databases, locational redundancy for Elastic Cloud Storage (ECS)

### Access Requirements
- Terms of Service of Coscine
- Provider can create access based on user enquiry
- Access limitation enforced by a set of keys and user roles in Coscine

## RWTH Hosted GitLab Instance Storage Services

### Service Overview
- Server for managing source code and other text-based data
- LFS enables version management of large binary data

### Documentation & Contact

- **Documentation URL**: https://docs.gitlab.com/ee/
- **Contact URL**: https://help.itc.rwth-aachen.de/service/ubrf9cmzd17m/
- **Contact E-Mail**: servicedesk@itc.rwth-aachen.de

### Confidentiality
- [Not provided]

### Storage Amount
- [Not provided]

### Access Protocols
- Standard GitLab access and protocols

### Connectivity and Accessibility
- [Not provided]

### Geo-Location and Privacy
- [Not provided]

### Regulation and Compliance
- [Not provided]

### Availability to Collaborators
- Eligible Users: Staff and students of RWTH Aachen University, collaboration partners (dedicated agreement may be necessary due to GitLab Education license terms)

### Persistence of Data
- [Not provided]

### Access Requirements
- Access based on RWTH Aachen University affiliation or collaboration agreements
