# [Storage Service Name] Storage Services

## Service Overview

- [Service description]

## Documentation & Contact

- **Documentation URL**: [Documentation URL for the Service]
- **Contact URL**: [Contact URL for the Service]
- **Contact E-Mail**: [Contact E-Mail for the Service]

## Confidentiality

- **Provider Access**: [Yes/No]
- **Access Limiting Mechanisms**:
  - [Mechanism 1]
  - [Mechanism 2]
  - [Mechanism 3]
  - [Additional mechanisms if needed]
- **User-side Encryption (Homomorphic Encryption)**:
  - [Possible/Not possible], [Support status]

## Storage Amount

- **Quota**: [Quota description]
- **Request and Application Process**: [Describe the application process that the users need to go through to use your service]
- **Cost**: [Cost description]

## Access Protocols

- **Protocol 1**: [Details]
- **Protocol 2**: [Details]
- **Additional protocols**: [Details]

## Connectivity and Accessibility (if applicable)

- **General Performance**: [Description]
- **Within [Institution/Organization]**: [Various protocols and details]
- **Outside [Institution/Organization]**: [Transfer-focused protocols and details]

## Geo-Location and Privacy

- **Data Centre**: [Location]; [Ownership and management]

## Regulation and Compliance

- **Compliance and Legal**: [Description]
- **Terms of Use**: [Link to terms of use]

## Availability to Collaborators

- **Eligible Users**: [Description of eligible users]
- **Data Persistence**: [Description of data persistence after user leaves institution/organization]

## Persistence of Data

- **Duration**: [Duration of data storage]
- **Deletion**: [Deletion policy and process]
